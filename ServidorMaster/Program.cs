﻿using System;
using System.Net.Sockets;

namespace ServidorMaster
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpListener servidor = new TcpListener(5000);
            servidor.Start();
            Socket conexao = servidor.AcceptSocket();
            Console.WriteLine("Conexão recebida");
            Console.ReadLine();
            servidor.Stop();
        }
    }
}
