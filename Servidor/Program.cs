﻿using System;
using System.Net.Sockets;

namespace Servidor
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient cliente = new TcpClient();
            cliente.Connect("localhost", 5000);
            Console.WriteLine("Conexão feita");
            Console.ReadLine();
            cliente.Close();
        }
    }
}
